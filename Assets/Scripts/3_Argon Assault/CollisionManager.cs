using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionManager : MonoBehaviour
{
    private PlayerControls playerControls;
    [SerializeField]
    private float sceneReloadDelay = 1.0f;
    [SerializeField]
    private ParticleSystem explosionVFX;

    private void Start()
    {
        playerControls = GetComponent<PlayerControls>();
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Player ship triggered by: " + other.tag.ToString());

        //Player disappears with explosion.
        explosionVFX.Play();
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<BoxCollider>().enabled = false;

        //Disable player controls.
        playerControls.enabled = false;

        Invoke("StartCrashSequence", sceneReloadDelay);
    }

    private void StartCrashSequence()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex);
    }
}