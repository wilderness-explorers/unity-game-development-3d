using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerControls : MonoBehaviour
{
    //Parameters - tuning.
    [Header("General settings")]
    [Tooltip("Player speed based on user input")]
    [SerializeField]
    private float playerSpeed = 1.0f;
    [Tooltip("How far player is allowed to move vertically and horizontally")]
    [SerializeField]
    private float clampRange = 5.0f;
    [Tooltip("Add all player lasers here")]
    [SerializeField]
    private GameObject[] lasers;

    [Header("Screen position based tuning")]

    [SerializeField]
    private float pitchFactorByPosition = -2.0f;
    [SerializeField]
    private float yawFactorByPosition = 5.0f;

    [Header("User input based tuning")]
    [SerializeField]
    private float pitchFactorByInput = -15.0f;
    [SerializeField]
    private float rollFactorByInput = -15.0f;

    private float horizontalInput, verticalInput;
    [SerializeField]
    private InputAction movement;
    [SerializeField]
    private InputAction fire;

    //Cache - references.

    //State variables.


    void Start()
    {
        
    }

    private void OnEnable()
    {
        movement.Enable();
        fire.Enable();
    }

    private void OnDisable()
    {
        movement.Disable();
        fire.Disable();
    }

    void Update()
    {
        TranslationMovement();
        RotationMovement();
        ProcessFiring();
    }

    private void TranslationMovement()
    {
        //Old Input system.
        /*
        float horizontalMovement = Input.GetAxis("Horizontal");
        float verticalMovement = Input.GetAxis("Vertical");
        */

        //New Input system.
        horizontalInput = movement.ReadValue<Vector2>().x;
        verticalInput = movement.ReadValue<Vector2>().y;

        float horizontalOffset = horizontalInput * playerSpeed * Time.deltaTime;
        float verticalOffset = verticalInput * playerSpeed * Time.deltaTime;

        float horizontalPosition = transform.localPosition.x + horizontalOffset;
        float verticalPosition = transform.localPosition.y + verticalOffset;

        float horizontalClamp = Mathf.Clamp(horizontalPosition, -clampRange, clampRange);
        float verticalClamp = Mathf.Clamp(verticalPosition, -clampRange, clampRange);

        transform.localPosition = new Vector3(horizontalClamp, verticalClamp, transform.localPosition.z);
    }

    private void RotationMovement()
    {
        //Pitch calculated based on position of player on screen and user input on the controller.
        float pitch = transform.localPosition.y * pitchFactorByPosition + verticalInput * pitchFactorByInput;
        float yaw = transform.localPosition.x * yawFactorByPosition;
        float roll = horizontalInput * rollFactorByInput;

        transform.localRotation = Quaternion.Euler(pitch, yaw, roll);
    }

    private void ProcessFiring()
    {
        if(fire.ReadValue<float>() > 0.5f)
        {
            Firing(true);
        }
        else
        {
            Firing(false);
        }
    }

    private void Firing(bool firingState)
    {
        foreach (GameObject laser in lasers)
        {
            var emissionModule = laser.GetComponent<ParticleSystem>().emission;
            emissionModule.enabled = firingState;
        }
    }
}
