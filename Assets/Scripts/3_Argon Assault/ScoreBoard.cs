using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreBoard : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private int playerScore = 0;

    //Cache
    TMP_Text scoreText;

    private void Start()
    {
        scoreText = GetComponent<TMP_Text>();
        scoreText.text = "START";
    }

    public void IncreaseScore(int increment)
    {
        playerScore += increment;
        scoreText.text = playerScore.ToString();
    }
}
