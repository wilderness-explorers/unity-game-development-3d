using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlayer : MonoBehaviour
{
    private void Awake()
    {
        //Singleton pattern.
        int musicPlayerCount = FindObjectsOfType<MusicPlayer>().Length;
        Debug.Log("Total music player instances: " + musicPlayerCount);

        if(musicPlayerCount > 1)
        {
            Debug.Log("More than one music player is present.");
            Destroy(this.gameObject);
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }
}
