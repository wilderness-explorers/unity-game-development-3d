using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private int scoreIncrement = 1;
    [SerializeField]
    private int enemyHealth = 1;
    [SerializeField]
    private GameObject enemyExplosion, enemyHit;

    //Cache
    private GameObject parentObject;
    private ScoreBoard scoreBoard;

    //State variables

    private void Start()
    {
        scoreBoard = FindObjectOfType<ScoreBoard>();
        Rigidbody rb = this.gameObject.AddComponent<Rigidbody>();
        rb.useGravity = false;
        parentObject = GameObject.FindWithTag("ParticlesHolder");
    }

    private void OnParticleCollision()
    {
        UpdateScore();

        if(enemyHealth < 1)
        {
            KillEnemy();
        }
    }

    private void UpdateScore()
    {
        //Update enemy health.
        enemyHealth--;

        //Enemy hit VFX.
        GetComponent<MeshRenderer>().material.color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
        var hitParticles = Instantiate(enemyHit, transform.position, Quaternion.identity);
        hitParticles.transform.parent = parentObject.transform;

        //Update player score.
        scoreBoard.IncreaseScore(scoreIncrement);
    }

    private void KillEnemy()
    {
        var explosionParticles = Instantiate(enemyExplosion, transform.position, Quaternion.identity);
        explosionParticles.transform.parent = parentObject.transform;
        Destroy(this.gameObject);
    }
}