using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestruct : MonoBehaviour
{
    [SerializeField]
    private float timeDelay = 1.0f;

    void Start()
    {
        Destroy(this.gameObject, timeDelay);
    }
}
