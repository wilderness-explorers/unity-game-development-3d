using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetLocator : MonoBehaviour
{
    [SerializeField]
    Transform target;

    [Tooltip("Parent object that holds weapon particle system.")]
    [SerializeField]
    Transform weapon;

    [SerializeField]
    ParticleSystem particle;

    [Tooltip("Range within which the weapon can shoot an enemy.")]
    [SerializeField]
    private float range = 15.0f;

    void Start()
    {
        //target = FindObjectOfType<EnemyMover>().transform;
    }

    void Update()
    {
        //To dynamically switch between targets.
        FindClosestTarget();
    }

    private void FindClosestTarget()
    {
        RealmRushEnemy[] realmRushEnemies;
        realmRushEnemies = FindObjectsOfType<RealmRushEnemy>();

        Transform closestTarget = null;

        float maxDistance = Mathf.Infinity;

        for(int i = 0; i < realmRushEnemies.Length; i++)
        {
            float distanceToTarget = Vector3.Distance(realmRushEnemies[i].transform.position, transform.position);

            if(distanceToTarget < maxDistance)
            {
                closestTarget = realmRushEnemies[i].transform;
                maxDistance = distanceToTarget;
            }
        }

        target = closestTarget;

        AimWeapon(target);
    }

    private void AimWeapon(Transform enemyTarget)
    {
        float targetDistance = Vector3.Distance(transform.position, enemyTarget.transform.position);

        weapon.LookAt(enemyTarget);

        if (targetDistance < range)
        {
            Attack(true);
        }
        else
        {
            Attack(false);
        }
    }

    private void Attack(bool isAttackable)
    {
        var particleEmission = particle.emission;
        particleEmission.enabled = isAttackable;
    }
}
