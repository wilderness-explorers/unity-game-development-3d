using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinder : MonoBehaviour
{
    //Parameters.
    [Tooltip("Define types of directions of search.")]
    private Vector2Int[] directions = { Vector2Int.right, Vector2Int.left, Vector2Int.up, Vector2Int.down };
    //private Vector2Int[] directions = { Vector2Int.right, Vector2Int.left, Vector2Int.up, Vector2Int.down, (Vector2Int.up + Vector2Int.right), (Vector2Int.down + Vector2Int.right), (Vector2Int.up + Vector2Int.left), (Vector2Int.down + Vector2Int.left) };

    [SerializeField]
    private Vector2Int startCoordinates;
    public Vector2Int StartCoordinates
    {
        get
        {
            return startCoordinates;
        }
    }

    [SerializeField]
    private Vector2Int destinationCoordinates;
    public Vector2Int DestinationCoordinates
    {
        get
        {
            return destinationCoordinates;
        }
    }

    private Node startNode;
    private Node destinationNode;
    private Node currentSearchNode;

    //To store nodes that have been explored.
    private Dictionary<Vector2Int, Node> reachedNodes = new Dictionary<Vector2Int, Node>();

    //To store the entire frontier - unexplored nodes.
    private Queue<Node> frontier = new Queue<Node>();

    //Cache.
    private GridManager gridManager;
    private Dictionary<Vector2Int, Node> gridReference = new Dictionary<Vector2Int, Node>();

    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();

        if(gridManager != null)
        {
            gridReference = gridManager.Grid;

            startNode = gridReference[startCoordinates];
            destinationNode = gridReference[destinationCoordinates];
        }
    }

    void Start()
    {
        GenerateNewPath();
    }

    public List<Node> GenerateNewPath()
    {
        return GenerateNewPath(startCoordinates);
    }

    //Method overload to generate path instantenously, when enemy is halfway throught the path already.
    public List<Node> GenerateNewPath(Vector2Int currentCoordinate)
    {
        gridManager.ResetNodes();

        BreadthFirstSearch(currentCoordinate);
        return GeneratePath();
    }

    private void ExploreNeighbours()
    {
        List<Node> neighbors = new List<Node>();

        foreach (Vector2Int direction in directions)
        {
            Vector2Int neighborCoords = currentSearchNode.coordinates + direction;

            if (gridReference.ContainsKey(neighborCoords))
            {
                neighbors.Add(gridReference[neighborCoords]);
            }
        }

        foreach (Node neighbor in neighbors)
        {
            if (!reachedNodes.ContainsKey(neighbor.coordinates) && neighbor.isWalkable)
            {
                neighbor.connectedTo = currentSearchNode;
                reachedNodes.Add(neighbor.coordinates, neighbor);
                frontier.Enqueue(neighbor);
            }
        }

    }

    private void BreadthFirstSearch(Vector2Int currentCoordinates)
    {
        //Start and End nodes should be walkable for enemies. Not placeable for Towers. This is an exception only for these 2 nodes.
        startNode.isWalkable = true;
        destinationNode.isWalkable = true;

        //To clear old data, whenever new path is generated.
        frontier.Clear();
        reachedNodes.Clear();

        //To come out of while loop.
        bool isRunning = true;

        //Enemy to recalculate path from its current position instead of its starting position.
        frontier.Enqueue(gridReference[currentCoordinates]);
        reachedNodes.Add(currentCoordinates, gridReference[currentCoordinates]);

        while(frontier.Count > 0 && isRunning)
        {
            currentSearchNode = frontier.Dequeue();
            currentSearchNode.isExplored = true;

            ExploreNeighbours();

            if(currentSearchNode.coordinates == destinationNode.coordinates)
            {
                isRunning = false;
            }
        }
    }

    //Build the path, by backtracking from the destination to the start node.
    private List<Node> GeneratePath()
    {
        List<Node> path = new List<Node>();
        Node currentNode = destinationNode;

        path.Add(currentNode);
        currentNode.isPath = true;
        Debug.Log("Connected node.");

        while (currentNode.connectedTo != null)
        {
            currentNode = currentNode.connectedTo;
            path.Add(currentNode);
            currentNode.isPath = true;
            Debug.Log("Connected nodes.");
        }

        path.Reverse();
        return path;
    }

    //To check if placing a tower will block the Path.
    public bool IsBlockingPath(Vector2Int coordinates)
    {
        if(gridReference.ContainsKey(coordinates))
        {
            //Store whatever previous state was. For safer side.
            bool previousState = gridReference[coordinates].isWalkable;

            gridReference[coordinates].isWalkable = false;
            List<Node> newPath = GenerateNewPath();

            //Reset to previous state.
            gridReference[coordinates].isWalkable = previousState;


            if(newPath.Count <= 1)
            {
                GenerateNewPath();
                return true;
            }
        }

        //if we couldnt find grid manager, we havent technically blocked the path.
        return false;
    }

    //Method responsible for sending out broadcast message. Pathfinder script is on parent object "Object pool". Enemies are its children.
    public void NotifyReceivers()
    {
        //Inform enemy to recalculate path from current position (Parameter - False). System shouldn't care if there are no listeners too.
        BroadcastMessage("RecalculatePath", false, SendMessageOptions.DontRequireReceiver);
    }
}
