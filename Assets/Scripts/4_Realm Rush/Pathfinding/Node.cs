using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Node
{
    [Tooltip("Coordinates, the node is going to occupy in our world/grid.")]
    public Vector2Int coordinates;

    [Tooltip("If the node is walkable or has obstacle.")]
    public bool isWalkable;

    [Tooltip("Whether node is explored by pathfinding algorithm or not.")]
    public bool isExplored;

    [Tooltip("Whether the node is in the path or not.")]
    public bool isPath;

    [Tooltip("Which node, the current node is connected to.")]
    public Node connectedTo;

    //Constructor.
    public Node(Vector2Int coordinates, bool isWalkable)
    {
        this.coordinates = coordinates;
        this.isWalkable = isWalkable;
    }
}