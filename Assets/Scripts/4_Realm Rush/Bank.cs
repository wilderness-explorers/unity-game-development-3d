using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Bank : MonoBehaviour
{
    [SerializeField]
    private int initialBalance = 150;
    [SerializeField]
    private int currentBalance;
    public int CurrentBalance
    {
        get
        {
            return currentBalance;
        }
    }

    private void Awake()
    {
        currentBalance = initialBalance;
    }

    //When enemy is destroyed.
    public void Deposit(int money)
    {
        currentBalance += Mathf.Abs(money);
    }

    //When enemy reaches target.
    public void Withdraw(int money)
    {
        currentBalance -= Mathf.Abs(money);

        if(currentBalance < 0)
        {
            ReloadLevel();
        }
    }

    public void ReloadLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(currentSceneIndex);
    }
}