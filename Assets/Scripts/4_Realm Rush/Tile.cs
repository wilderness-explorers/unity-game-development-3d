using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Tile : MonoBehaviour,IPointerClickHandler
{
    //Parameters
    [SerializeField]
    private Tower towerPrefab;

    //States
    [SerializeField]
    private bool isPlaceable;
    //Property
    public bool IsPlaceable
    {
        get
        {
            return isPlaceable;
        }
    }

    private GridManager gridManager;
    private PathFinder pathFinder;

    private Vector2Int coordinates = new Vector2Int();

    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();
        pathFinder = FindObjectOfType<PathFinder>();
    }

    //To tell grid manager whether this tile is blocked or not.
    //Block node in Grid manager expects coordinates.
    private void Start()
    {
        if(gridManager != null)
        {
            //Get coordinates of this tile's position.
            coordinates = gridManager.GetCoordinatesFromPosition(transform.position);

            if(isPlaceable == false)
            {
                Debug.Log("isPlaceable is false.");
                gridManager.BlockNode(coordinates);
            }
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //To fully work with Grid manager and Path finder.
        if(gridManager.GetNode(coordinates).isWalkable == true && pathFinder.IsBlockingPath(coordinates) == false)
        {
            bool isSuccessful = towerPrefab.CreateTower(towerPrefab, transform.position);

            if(isSuccessful)
            {
                gridManager.BlockNode(coordinates);

                //To tell enemy mover, that tower placement is successful. To recalculate it's path.
                //Tell pathfinder first. Pathfinder then informs enemy mover.
                //Broadcast message.
                pathFinder.NotifyReceivers();
            }
        }
    }
}
