using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.InputSystem;

[ExecuteAlways]
[RequireComponent(typeof(TextMeshPro))]
public class CoordinatesLabeller : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private Color defaultColor = Color.white;
    [SerializeField]
    private Color blockedColor = Color.gray;

    [SerializeField]
    private Color exploredColor = Color.yellow;
    [SerializeField]
    private Color pathColor = new Color(1f, 0f, 0.5f);

    [Tooltip("To show labels in edit mode.")]
    [SerializeField]
    private bool showLabels;

    //Cache
    private TextMeshPro coordinateLabel;
    private Vector2Int cubeCoordinates;

    //Disabling waypoints to use Grid manager - Pathfinding algorithms.
    //private Waypoints waypoints;
    
    [SerializeField]
    private InputAction toggle;

    private GridManager gridManager;

    private void Awake()
    {
        gridManager = FindObjectOfType<GridManager>();

        coordinateLabel = GetComponent<TextMeshPro>();
        coordinateLabel.enabled = false;

        //To display labels in play mode.
        LabelCoordinates();

        //waypoints = GetComponentInParent<Waypoints>();

        toggle.performed += y => ToggleLabels();
    }

    void Update()
    {
        //Code to execute only in edit mode.
        if (!Application.isPlaying)
        {
            LabelCoordinates();
            UpdateObjectName();

            coordinateLabel.enabled = showLabels;
        }

        //Code to execute only in play mode.
        SetLabelColor();
    }

    private void OnEnable()
    {
        toggle.Enable();
    }

    private void OnDisable()
    {
        toggle.Disable();
    }

    private void LabelCoordinates()
    {
        if(gridManager == null)
        {
            return;
        }

        //This script to be placed in "Editor" folder before build.
        cubeCoordinates.x = Mathf.RoundToInt(transform.parent.position.x / UnityEditor.EditorSnapSettings.move.x);
        cubeCoordinates.y = Mathf.RoundToInt(transform.parent.position.z / UnityEditor.EditorSnapSettings.move.z);

        coordinateLabel.text = $"{cubeCoordinates.x},{cubeCoordinates.y}";
    }

    private void UpdateObjectName()
    {
        transform.parent.name = cubeCoordinates.ToString();
    }

    private void SetLabelColor()
    {
        if(gridManager == null)
        {
            return;
        }

        //To find the node from the dictionary, for this particular instance of coordinate labeller.
        Node node = gridManager.GetNode(cubeCoordinates);

        if(node == null)
        {
            return;
        }

        //Order of priority.
        //1. To check if a node is walkable.
        //2. Check if it is in the Path.
        //3. Explored or not.
        if (!node.isWalkable)
        {
            coordinateLabel.color = blockedColor;
        }
        else if(node.isPath)
        {
            coordinateLabel.color = pathColor;
        }
        else if(node.isExplored)
        {
            coordinateLabel.color = exploredColor;
        }
        else
        {
            coordinateLabel.color = defaultColor;
        }

        /*
        if (waypoints.IsPlaceable == true)
        {
            coordinateLabel.color = defaultColor;
        }
        else
        {
            coordinateLabel.color = blockedColor;
        }
        */
    }

    private void ToggleLabels()
    {
        coordinateLabel.enabled = !coordinateLabel.IsActive();
    }
}