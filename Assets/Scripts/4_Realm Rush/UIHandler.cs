using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIHandler : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private TMP_Text scoreText;

    private Bank bank;

    private void Awake()
    {
        bank = FindObjectOfType<Bank>();
    }

    private void Update()
    {
        scoreText.text = "GOLD: " + bank.CurrentBalance.ToString();
    }
}
