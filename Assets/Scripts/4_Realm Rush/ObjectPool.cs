using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField]
    private GameObject enemyPrefab;
    [SerializeField]
    [Range(0.1f, 30.0f)]
    private float spawnTime = 1.0f;
    [SerializeField]
    [Range(0, 50)]
    private int poolSize;

    private GameObject[] pool;

    private GridManager gridManager;
    private PathFinder pathFinder;

    private void Awake()
    {
        PopulatePool();

        gridManager = FindObjectOfType<GridManager>();
        pathFinder = FindObjectOfType<PathFinder>();
    }

    void Start()
    {
        StartCoroutine(InstantiateEnemy());
    }

    private void Update()
    {
        
    }

    private void PopulatePool()
    {
        pool = new GameObject[poolSize];

        for(int i = 0; i < pool.Length; i++)
        {
            pool[i] = Instantiate(enemyPrefab, transform);
            pool[i].SetActive(false);
        }
    }

    private IEnumerator InstantiateEnemy()
    {
        while (true)
        {
            EnableObjectInPool();
            yield return new WaitForSeconds(spawnTime);
        }
    }

    private void EnableObjectInPool()
    {
        for(int i = 0; i < pool.Length; i++)
        {
            if(!pool[i].activeInHierarchy)
            {
                pool[i].transform.position = gridManager.GetPositionFromCoordinates(pathFinder.StartCoordinates);
                pool[i].SetActive(true);
                return;
            }
        }
    }
}