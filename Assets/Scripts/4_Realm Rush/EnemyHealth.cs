using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Dependent on Enemy script. To avoid errors, attribute added.
[RequireComponent(typeof (RealmRushEnemy))]
public class EnemyHealth : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private int maxEnemyHealth = 2;

    [Tooltip("Enemy health increases by this value everytime enemy dies.")]
    [SerializeField]
    private int difficultyRamp = 1;

    private int currentEnemyHealth = 1;

    //Cache
    private RealmRushEnemy realmRushEnemy;

    private void Awake()
    {
        realmRushEnemy = GetComponent<RealmRushEnemy>();
    }

    private void OnEnable()
    {
        currentEnemyHealth = maxEnemyHealth;
    }

    private void OnParticleCollision(GameObject other)
    {
        ProcessHit();
    }

    private void ProcessHit()
    {
        Debug.Log("Enemy hit!");
        currentEnemyHealth--;
        if (currentEnemyHealth < 1)
        {
            //Destroy(this.gameObject);
            this.gameObject.SetActive(false);

            //To increase difficulty.
            maxEnemyHealth += difficultyRamp;

            realmRushEnemy.DepositGold();
        }
    }
}
