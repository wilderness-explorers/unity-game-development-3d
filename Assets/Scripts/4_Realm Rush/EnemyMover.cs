using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMover : MonoBehaviour
{
    //Parameters
    private List<Node> path = new List<Node>();
    [SerializeField]
    [Range(0.0f, 5.0f)] private float speed = 1.0f;

    //Cache
    private RealmRushEnemy realmRushEnemy;

    private PathFinder pathFinder;
    private GridManager gridManager;

    private void Awake()
    {
        realmRushEnemy = GetComponent<RealmRushEnemy>();
        gridManager = FindObjectOfType<GridManager>();
        pathFinder = FindObjectOfType<PathFinder>();
    }

    void OnEnable()
    {
        //To regenerate path from starting position.
        RecalculatePath(true);

        ReturnToStartingPosition();
    }

    private void RecalculatePath(bool fromStartPosition)
    {
        Vector2Int tempCoordinates;

        //True, if we need to generate path from starting position. False, if path is from current position.
        if(fromStartPosition)
        {
            tempCoordinates = pathFinder.StartCoordinates;
        }
        else
        {
            tempCoordinates = gridManager.GetCoordinatesFromPosition(transform.position);
        }

        //Stop the enemy from moving, until path recalculation.
        StopAllCoroutines();

        //To clear the list everytime a new path is created.
        path.Clear();

        path = pathFinder.GenerateNewPath(tempCoordinates);

        StartCoroutine(FollowPath());
    }

    private void ReturnToStartingPosition()
    {
        transform.position = gridManager.GetPositionFromCoordinates(pathFinder.StartCoordinates);
    }

    private IEnumerator FollowPath()
    {
        //i is set to 1, so the enemy does not stay still at current position. Always moves towards the next node.
        for (int i = 1; i < path.Count; i++)
        {
            Vector3 startingPosition = transform.position;

            //End position will be next node in our path.
            Vector3 endingPosition = gridManager.GetPositionFromCoordinates(path[i].coordinates);

            //For the enemy to face the forward direction.
            transform.LookAt(endingPosition);
            
            float travelPercent = 0.0f;

            while (travelPercent < 1.0f)
            {
                travelPercent += Time.deltaTime * speed;

                //For smooth movement between way points.
                transform.position = Vector3.Lerp(startingPosition, endingPosition, travelPercent);

                yield return new WaitForEndOfFrame();
            }
        }

        FinishLine();
    }

    private void FinishLine()
    {
        this.gameObject.SetActive(false);
        realmRushEnemy.StealGold();
    }
}
