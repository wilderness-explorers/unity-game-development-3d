using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Waypoints : MonoBehaviour,IPointerClickHandler
{
    //Parameters
    [SerializeField]
    private Tower towerPrefab;

    //States
    [SerializeField]
    private bool isPlaceable;
    //Property
    public bool IsPlaceable
    {
        get
        {
            return isPlaceable;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if(isPlaceable)
        {
            Debug.Log(transform.name);

            towerPrefab.CreateTower(towerPrefab, transform.position);
            //Instantiate(towerPrefab, transform.position, Quaternion.identity);
            
            isPlaceable = false;
        }
    }
}
