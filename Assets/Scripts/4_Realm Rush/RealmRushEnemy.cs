using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RealmRushEnemy : MonoBehaviour
{
    //Parameters
    [SerializeField]
    private int goldReward = 25;
    [SerializeField]
    private int goldPenalty = 25;

    //Cache
    private Bank bank;

    private void Awake()
    {
        bank = FindObjectOfType<Bank>();
    }

    public void DepositGold()
    {
        if(bank == null)
        {
            return;
        }

        bank.Deposit(goldReward);
    }

    public void StealGold()
    {
        if (bank == null)
        {
            return;
        }

        bank.Withdraw(goldPenalty);
    }
}
