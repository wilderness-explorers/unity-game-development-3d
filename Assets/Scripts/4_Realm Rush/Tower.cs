using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    [Tooltip("Cost to place a tower.")]
    [SerializeField]
    private int cost = 75;

    [SerializeField]
    private float buildTime = 5.0f;

    public void Start()
    {
        StartCoroutine(StartBuilding());
    }

    public bool CreateTower(Tower tower, Vector3 position)
    {
        Bank bank;

        bank = FindObjectOfType<Bank>();

        if(bank == null)
        {
            return false;
        }

        if(bank.CurrentBalance >= cost)
        {
            Instantiate(tower.gameObject, position, Quaternion.identity);
            bank.Withdraw(cost);
            return true;
        }        

        return false;
    }

    private IEnumerator StartBuilding()
    {
        //To avoid disabling game object in hierarchy.
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(false);

            foreach (Transform grandChildren in transform)
            {
                grandChildren.gameObject.SetActive(false);
            }
        }

        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
            yield return new WaitForSeconds(buildTime);

            foreach (Transform grandChildren in transform)
            {
                grandChildren.gameObject.SetActive(true);
                yield return new WaitForSeconds(buildTime);
            }
        }
    }
}