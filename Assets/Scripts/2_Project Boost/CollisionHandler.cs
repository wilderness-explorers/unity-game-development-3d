﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CollisionHandler : MonoBehaviour
{
    //Parameters - for tuning.
    [SerializeField]
    private float delayTime = 1.0f;
    [SerializeField]
    private AudioClip crashSound, landingSuccess;
    [SerializeField]
    private ParticleSystem crashParticles, successParticles;

    //Cache - references.
    private Movement movement;
    private AudioSource audioSource;

    //State - instance variables.
    bool isTransitioning = false;
    bool deactivateColliders = false;

    private void Start()
    {
        movement = this.GetComponent<Movement>();
        audioSource = this.GetComponent<AudioSource>();
    }

    private void Update()
    {
        DebugGame();
    }

    private void DebugGame()
    {
        //For debug.
        if (Input.GetKeyDown(KeyCode.L))
        {
            LoadNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.C))
        {
            deactivateColliders = !deactivateColliders;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(isTransitioning == true || deactivateColliders == true)
        {
            return;
        }
        
        switch(collision.transform.tag)
        {
            case "Friendly":
                Debug.Log("Launching pad.");
                break;
            case "Finish":
                Debug.Log("Finish line.");
                LandingSequence();
                break;
            default:
                Debug.Log("You blew up.");
                CrashSequence();
                break;
        }
    }

    private void LandingSequence()
    {
        isTransitioning = true;

        audioSource.PlayOneShot(landingSuccess, 1.0f);
        successParticles.Play();

        movement.enabled = false;
        Invoke("LoadNextLevel", delayTime);
    }

    private void CrashSequence()
    {
        isTransitioning = true;

        audioSource.PlayOneShot(crashSound, 1.0f);
        crashParticles.Play();

        movement.enabled = false;
        Invoke("ReloadLevel", delayTime);
    }

    private void ReloadLevel()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentScene);
    }

    private void LoadNextLevel()
    {
        int currentScene = SceneManager.GetActiveScene().buildIndex;
        int nextScene = currentScene + 1;
        
        //To return back to level 1, after last level.
        if(nextScene == SceneManager.sceneCountInBuildSettings)
        {
            nextScene = 0;
        }

        SceneManager.LoadScene(nextScene);
    }
}