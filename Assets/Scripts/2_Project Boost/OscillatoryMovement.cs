using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscillatoryMovement : MonoBehaviour
{
    private Vector3 startingPosition;
    [SerializeField]
    private Vector3 movementVector;
    [SerializeField] [Range(0,1)]
    private float movementFactor;
    [SerializeField]
    private float period = 2.0f;

    void Start()
    {
        startingPosition = this.transform.position;
    }

    void Update()
    {
        //Epsilon is smallest float number measurable by unity.
        if(period <= Mathf.Epsilon)
        {
            return;
        }

        //To apply a Sine function behaviour to oscillatory motion.
        //Cycles value increases with time. Higher value of period produces slow movement of obstacle.
        float cycles = Time.time / period;

        //Radian is angle subtended by an arc of length = radius of the circle.
        //3.14 radians or 1 Pi makes a semicircle. 2 Pi makes a complete circle (Also written as tau).
        const float tau = Mathf.PI * 2;

        //Following gives value in range -1 to 1 (Amplitude of Sine wave).
        movementFactor = Mathf.Sin(cycles * tau);

        //To limit value of movement factor from 0 to 1.
        Vector3 offset = movementVector * Mathf.Abs(movementFactor);
        transform.position = startingPosition + offset;
    }
}
