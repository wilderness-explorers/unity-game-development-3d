﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    //Parameters - for tuning.
    [SerializeField]
    float thrust = 1.0f, rotationSpeed = 1.0f;
    [SerializeField]
    private AudioClip engineSound;
    [SerializeField]
    private ParticleSystem maintThrustParticles, leftThrustParticles, rightThrustParticles;

    //Cache - references.
    Rigidbody rb;
    AudioSource audioSource;

    //State - instance variables.


    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        audioSource = this.GetComponent<AudioSource>();
    }

    void Update()
    {
        ThrustControl();
        RotationControl();
    }

    void ThrustControl()
    {
        if(Input.GetKey(KeyCode.UpArrow))
        {
            ForwardThrust();
        }
        else
        {
            StopThrust();
        }
    }

    void RotationControl()
    {
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            RotateLeft();
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            RotateRight();
        }
        else
        {
            StopRotation();
        }
    }

    private void ForwardThrust()
    {
        rb.AddRelativeForce(Vector3.up * thrust * Time.deltaTime);

        if (audioSource.isPlaying == false)
        {
            audioSource.PlayOneShot(engineSound, 1.0f);
        }

        if (maintThrustParticles.isPlaying == false)
        {
            maintThrustParticles.Play();
        }
    }

    private void StopThrust()
    {
        audioSource.Stop();
        maintThrustParticles.Stop();
    }

    private void RotateLeft()
    {
        //Rotate rocket towards its left.
        CalculateRotation(rotationSpeed);

        //Particle effect.
        if (leftThrustParticles.isPlaying == false)
        {
            leftThrustParticles.Play();
        }
    }

    private void RotateRight()
    {
        //Rotate rocket towards its right.
        CalculateRotation(-rotationSpeed);

        //Particle effect.
        if (rightThrustParticles.isPlaying == false)
        {
            rightThrustParticles.Play();
        }
    }

    private void StopRotation()
    {
        rightThrustParticles.Stop();
        leftThrustParticles.Stop();
    }

    void CalculateRotation(float rotationFactor)
    {
        //Freezing rigid body(Physics) rotation so it can be controlled by user.
        rb.freezeRotation = true;

        transform.Rotate(Vector3.forward * rotationFactor * Time.deltaTime);

        //Physics system takes over again.
        rb.freezeRotation = false;
    }
}
