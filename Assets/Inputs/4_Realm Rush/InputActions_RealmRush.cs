// GENERATED AUTOMATICALLY FROM 'Assets/Inputs/4_Realm Rush/InputActions_RealmRush.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @InputActions_RealmRush : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @InputActions_RealmRush()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""InputActions_RealmRush"",
    ""maps"": [
        {
            ""name"": ""UI"",
            ""id"": ""3d8364b9-877d-4599-ac12-184e396e7848"",
            ""actions"": [
                {
                    ""name"": ""Toggle"",
                    ""type"": ""Button"",
                    ""id"": ""9bf04ec9-9f87-4ead-b2bd-a64ad50493f3"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""d21bc04f-c93c-48ac-a4f6-a3f81e5d4608"",
                    ""path"": ""<Keyboard>/ctrl"",
                    ""interactions"": ""Press"",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Toggle"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_Toggle = m_UI.FindAction("Toggle", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_Toggle;
    public struct UIActions
    {
        private @InputActions_RealmRush m_Wrapper;
        public UIActions(@InputActions_RealmRush wrapper) { m_Wrapper = wrapper; }
        public InputAction @Toggle => m_Wrapper.m_UI_Toggle;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @Toggle.started -= m_Wrapper.m_UIActionsCallbackInterface.OnToggle;
                @Toggle.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnToggle;
                @Toggle.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnToggle;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Toggle.started += instance.OnToggle;
                @Toggle.performed += instance.OnToggle;
                @Toggle.canceled += instance.OnToggle;
            }
        }
    }
    public UIActions @UI => new UIActions(this);
    public interface IUIActions
    {
        void OnToggle(InputAction.CallbackContext context);
    }
}
